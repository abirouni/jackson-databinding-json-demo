package com.jackson.json.demo;

import java.io.File;
import com.fasterxml.jackson.databind.ObjectMapper;

public class Driver {

	public static void main(String[] args) {

		try {
			
			// cretate object mapper
			ObjectMapper mapper = new ObjectMapper();

			// read Json file and map/convert to java POJO
			Student student = mapper.readValue(new File("data/sample-full.json"), Student.class);
			System.out.println("first name : " + student.getFirstName());
			System.out.println("last name : " + student.getLastName());
			Address address= student.getAddress();
			System.out.println("street : " + address.getStreet());
			System.out.println("City  : " + address.getCity());
			
			for (String string : student.getLanguages()) {
				System.out.println("languages : " + string);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
